import ssl
from http import HTTPStatus
import OpenSSL
from datetime import datetime
from django.http import JsonResponse
from django.views.decorators.http import require_GET


@require_GET
def get_cert_view(request, domain='google.com'):
    try:
        cert = ssl.get_server_certificate((domain, 443))
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
        pk = x509.get_pubkey()
        key_length = pk.bits()
        key_expire = datetime.strptime(x509.get_notAfter().decode('ascii'), '%Y%m%d%H%M%SZ').date()
        signature = x509.get_signature_algorithm().decode('utf-8')
        serial = hex(x509.get_serial_number())

        return JsonResponse(
            {'key_length': key_length, 'key_expire': key_expire, 'signature': signature,
             'serial': serial},
            status=HTTPStatus.OK, content_type="application/json")

    except ssl.SSLError:
        return JsonResponse({'key_length': 'key not found'}, status=HTTPStatus.BAD_REQUEST,
                            content_type="application/json")


def hello_view(request):
    return JsonResponse({'test': 'Bonjour Humain'}, content_type="application/json")
