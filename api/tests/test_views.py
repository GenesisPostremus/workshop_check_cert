import json
import ssl
from http import HTTPStatus

import OpenSSL
import django.test
from django.test import TestCase
from django.urls import reverse


class TestView(TestCase):

    @classmethod
    def setUpClass(cls):
        client = django.test.Client
        super().setUpClass()

    def test_get_cert_view_accept_get(self):
        response = self.client.get(reverse("check_cert", kwargs={"domain": "test.fr"}))

        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_view_check_cert_refuse_post(self):
        response = self.client.post(reverse("check_cert", kwargs={"domain": "test.fr"}), data={})

        self.assertEqual(response.status_code, HTTPStatus.METHOD_NOT_ALLOWED)

    def test_view_check_cert_return_json(self):
        response = self.client.get(reverse("check_cert", kwargs={"domain": "gnupg.org"}))

        self.assertEqual(json.loads(response.content), {'key_length': 2048})

    def test_view_check_cert_return_bad_request_for_http_domain(self):
        response = self.client.get(reverse("check_cert", kwargs={"domain": "glossaire.infowebmaster.fr"}))

        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
