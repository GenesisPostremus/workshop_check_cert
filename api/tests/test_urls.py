from django.test import TestCase
from django.urls import resolve, reverse

from api.views import get_cert_view


class TestUrls(TestCase):
    def test_url_check_cert(self):
        response = resolve(reverse("check_cert", kwargs={"domain": "test.fr"}))

        self.assertEqual(response.func, get_cert_view)
